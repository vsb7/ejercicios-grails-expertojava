package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib: 'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo'

    //Creamos etiqueta para ejercicio
    def printIconFromBoolean = { value ->
            def valor = value['value']
            if (valor) {
                out << asset.image(src: 'done.png')
            } else {
                out << asset.image(src: 'undone.png')
            }
        }

    def includeJs = {attrs ->
        out << "<script src='scripts/${attrs['script']}.js'></script>"
    }


}
