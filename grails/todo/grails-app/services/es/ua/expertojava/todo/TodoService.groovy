package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory


//@Transactional
class TodoService {

    def deleteTodo(Todo todoInstance) {

        //Funciona porque lo estás borrando de la tabla muchos a muchos
        todoInstance.tags.each { tag -> todoInstance.removeFromTags(tag) }
        todoInstance.delete flush:true
    }


    def saveTodo (Todo todoInstance){
        if(todoInstance.done){
            todoInstance.dateDone = new Date()
        }

        todoInstance.save flush:true
    }

    def lastTodosDone(Integer hours){

        def time
        use( TimeCategory ) {
            time = new Date() - hours.hours
        }

        return Todo.findAllByDateDoneGreaterThan(time)
    }

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }
}
