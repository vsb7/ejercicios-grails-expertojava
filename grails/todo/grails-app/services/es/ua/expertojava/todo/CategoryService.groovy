package es.ua.expertojava.todo

import grails.transaction.Transactional

//@Transactional
class CategoryService {

    def deleteCategory(Category categoryInstance) {

        /*
        * - Seleccionamos los todos filtrados por categoría
        * - Los tags son borrados en cascada debido a la relación entre todo y tag
        * */
        Todo.findAllByCategory(categoryInstance)?.each{
            todo-> todo.category=null
            todo.save(flush:true)
        }

        categoryInstance.delete flush:true
    }
}
