<div id="header">
<div id="menu">
    <nobr>
        <!--
        <g:if test="${isUserLoggedIn}">
            <b>${userInstance?.name} ${userInstance?.surnames}</b> |
            <g:link controller="user" action="logout">Logout</g:link>
        </g:if>
        <g:else>
            <g:link controller="user" action="login">Login</g:link>
        </g:else>
        -->

        <sec:ifLoggedIn>
            <sec:loggedInUserInfo field="username"/>

            <form method="POST" action="${createLink(controller:'logout') }">
                <input type="submit" name="Logout" value="Logout"/>
            </form>

            <!--<g:link controller="user" action="logout">Logout</g:link>-->
        </sec:ifLoggedIn>

        <sec:ifNotLoggedIn>
            <a href="http://localhost:8080/todo/login/auth">Login</a>
            <!-- <g:link controller="user" action="login">Login</g:link> -->
        </sec:ifNotLoggedIn>

    </nobr>
</div>
</div>

