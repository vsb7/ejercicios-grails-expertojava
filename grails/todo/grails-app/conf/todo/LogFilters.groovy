package todo

class LogFilters {

    def springSecurityService

    def userTmp

    def filters = {
        all(controller:'todo|category|tag|user', action:'create|edit|index|show|showTodosByUser|listByCategory|showListByCategory|listNextTodos') {
            before = {
            }
            after = { Map model ->
                userTmp = springSecurityService.currentUser
                log.trace ("- User ${userTmp} - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}


