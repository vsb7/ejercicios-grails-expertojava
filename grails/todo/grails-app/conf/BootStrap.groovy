import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {
            //Añadir usuarios ------------------------------------------------------

            Role roleAdmin=new Role(authority: "ROLE_ADMIN").save()
            Role role=new Role(authority: "ROLE_BASIC").save()

            User user1=new User(username: "usuario1", password: "usuario1", name:"usuario1",
                    surnames:"usuario1", email:"usuario1@usuario1.com", confirmPassword:"usuario1")
            user1.save()

            User user2=new User(username: "usuario2", password: "usuario2", name:"usuario2",
                    surnames:"usuario2", email:"usuario2@usuario2.com", confirmPassword:"usuario2")
            user2.save()

            User userAdmin=new User(username: "admin", password: "admin", name:"admin",
                    surnames:"admin", email:"admin@admin.com", confirmPassword:"admin")
            userAdmin.save()

            PersonRole.create(user1,role).save()
            PersonRole.create(user2,role).save()
            PersonRole.create(userAdmin,roleAdmin).save()

            //------------------------
            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil", color: "#FFF222").save()
            def tagDifficult = new Tag(name: "Difícil", color: "#FFF333").save()
            def tagArt = new Tag(name: "Arte", color: "#FFF444").save()
            def tagRoutine = new Tag(name: "Rutina", color: "#FFF555").save()
            def tagKitchen = new Tag(name: "Cocina", color: "#FFF666").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1,user:user1)
            def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2,user:user2)
            def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4,user:user1)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), done: true,user:user2)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()

        }catch(Exception ex){
            println ex
        }
    }
    def destroy = {
    }
}
