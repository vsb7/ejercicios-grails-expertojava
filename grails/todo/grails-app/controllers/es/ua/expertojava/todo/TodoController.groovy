package es.ua.expertojava.todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    def todoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    //Pagina mostrar lista de tareas por usuario
    def showTodosByUser(Integer max){
        params.max = Math.min(max ?: 10, 100)

        def lista = Todo.findAllByUserInList(User.findAllByNameLike(params.username))

        respond lista, model:[todoInstanceCount: lista.size()]
    }

    //Pagina de seleccion de categorías
    def listByCategory(Integer max){
        params.max = Math.min(max ?: 10, 100)
        respond Category.list(params), model:[categoryInstanceCount: Category.count()]
    }

    def showListByCategory(Integer max){
        params.max = Math.min(max ?: 10, 100)

        def listaTareas = Todo.findAllByCategoryInList(Category.getAll(params?.category))

        listaTareas.sort{a,b-> b.date<=>a.date}

        respond listaTareas, model:[todoInstanceCount: Todo.count()]
    }

    //Añadido según apuntes
    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model:[todoInstanceCount: todoService.countNextTodos(days)],
                view:"index"
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        respond Todo.list(params), model:[todoInstanceCount: Todo.count()]
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.deleteTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
