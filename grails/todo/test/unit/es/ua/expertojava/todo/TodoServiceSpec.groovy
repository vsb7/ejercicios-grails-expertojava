package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
        def user1 = new User(username: "usuario1", password: "usuario1", name:"usuario1",
                surnames:"usuario1", email:"usuario1@usuario1.com", confirmPassword:"usuario1")
        def user2 = new User(username: "usuario2", password: "usuario2", name:"usuario2",
                surnames:"usuario2", email:"usuario2@usuario2.com", confirmPassword:"usuario2")
        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2, user:user1)
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1, user:user1 )
        def todoToday = new Todo(title:"Todo today", date: new Date(), user:user1)
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1, user:user2 )
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2, user:user2 )
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 3, user:user2 )
        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def nextTodos = service.getNextTodos(2,[:])
        expect:
        Todo.count() == 6
        and:
        nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
        nextTodos.size() == 2
        and:
        !nextTodos.contains(todoDayBeforeYesterday)
        !nextTodos.contains(todoToday)
        !nextTodos.contains(todoYesterday)
        !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }

    void "El método countNextTodos devuelve el número de tareas entre hoy y los días especificados por parámetro"() {
        given:
        def user1 = new User(username: "usuario1", password: "usuario1", name:"usuario1",
                surnames:"usuario1", email:"usuario1@usuario1.com", confirmPassword:"usuario1")
        def user2 = new User(username: "usuario2", password: "usuario2", name:"usuario2",
                surnames:"usuario2", email:"usuario2@usuario2.com", confirmPassword:"usuario2")
        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2,user: user1 )
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1,user: user1 )
        def todoToday = new Todo(title:"Todo today", date: new Date(),user: user1)
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1 ,user: user2)
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2 ,user: user2)
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 3,user: user2 )
        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def countTodos = service.countNextTodos(3)
        expect:
        countTodos == 3 //desde today hasta todoDayAfterDayAfterTomorrow
    }

    void "El método saveTodo() se encarga de almacenar la nueva instancia"(){
        given:
        def user1 = new User(username: "usuario1", password: "usuario1", name:"usuario1",surnames:"usuario1", email:"usuario1@usuario1.com",
                confirmPassword:"usuario1")

        def todo = new Todo(title:"TodoTest", date: new Date(), done:true,user: user1)
        and:
        mockDomain(Todo,[todo])
        and:
        todo.dateDone == null
        and:
        service.saveTodo(todo)
        expect:
        todo.dateDone != null
        todo.title.equals("TodoTest")
    }

}
