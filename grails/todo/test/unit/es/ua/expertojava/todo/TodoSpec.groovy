package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {

    }

    def cleanup() {
    }

    void "test something"() {
    }

    /*
    * La fecha de recordatorio nunca puede ser posterior a la fecha de la propia tarea
    * */
    def "La fecha de recordatorio nunca puede ser posterior a la fecha de la propia tarea"() {
        given:
        def user1 = new User(username: "usuario1", password: "usuario1", name:"usuario1",surnames:"usuario1", email:"usuario1@usuario1.com",
                confirmPassword:"usuario1")

        def c1 = new Todo(date:new Date(),reminderDate:new Date()+1,user:user1)
        when:
        c1.validate()
        then:
        c1?.errors['reminderDate']
    }

    def "Si la fecha de recordatorio es anterior a la fecha de la tarea todo correcto"() {
        given:
        def user1 = new User(username: "usuario1", password: "usuario1", name:"usuario1",surnames:"usuario1", email:"usuario1@usuario1.com",
                confirmPassword:"usuario1")
        def c1 = new Todo(date:new Date()+1,reminderDate:new Date(),user:user1)
        when:
        c1.validate()
        then:
        !c1?.errors['reminderDate']
    }



}
