package es.ua.expertojava.todo



import grails.test.mixin.*
import spock.lang.*

@TestFor(TodoController)
@Mock(Todo)
class TodoControllerSpec extends Specification {

    def todoService = new TodoService()

    def setup(){
        controller.todoService = todoService
    }

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        params["description"] = 'Description'
        params["title"] = 'Title tarea'
        params["date"] = new Date()
        params["reminderDate"] = new Date() - 1
        params["url"]
        params["category"] = new Category(name: 'Pintar habitación', description: "Pintar la habitación")
        params["done"] = false
        params["dateDone"] = null
        params["user"] = new User(username: "usuario1", password: "usuario1", name:"usuario1",
                surnames:"usuario1", email:"usuario1@usuario1.com", confirmPassword:"usuario1");
    }

    //Probamos los métodos listNextTodos(Integer days) y listByCategory()
    void "Test the listNextTodos action returns the correct model"() {
        given:
        def user1 = new User(username: "usuario1", password: "usuario1", name:"usuario1",
                surnames:"usuario1", email:"usuario1@usuario1.com", confirmPassword:"usuario1")
        def user2 = new User(username: "usuario2", password: "usuario2", name:"usuario2",
                surnames:"usuario2", email:"usuario2@usuario2.com", confirmPassword:"usuario2")
        // Copiado del bootstrap
        def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1,user: user1).save(flush:true)
        def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2,user: user1).save(flush:true)
        def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4,user: user2).save(flush:true)
        def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), done: true,user: user2).save(flush:true)

        when:"The index action is executed"
        controller.listNextTodos(1)

        then:
        Todo.count() == 4

        then:
        model.todoInstanceList.containsAll([todoPaintKitchen])
        !model.todoInstanceList.containsAll([todoWriteUnitTests,todoBakeCake,todoCollectPost])
        model.todoInstanceCount == 1
        view == "index"
    }

    def "Test method listByCategory"() {
        given:
        def categoryHome = new Category(name: "Hogar").save(flush:true)
        def categoryJob = new Category(name: "Trabajo").save(flush:true)
        when:
        controller.listByCategory()
        then:
        model.categoryInstanceList.containsAll([categoryHome,categoryJob])
    }

    // ------------------------------------------------------------------------------------

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.todoInstanceList
            model.todoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.todoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def todo = new Todo()
            todo.validate()
            controller.save(todo)

        then:"The create view is rendered again with the correct model"
            model.todoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params)

            controller.save(todo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/todo/show/1'
            controller.flash.message != null
            Todo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.show(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.edit(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/todo/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def todo = new Todo()
            todo.validate()
            controller.update(todo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.todoInstance == todo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params).save(flush: true)
            controller.update(todo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/todo/show/$todo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when: "The delete action is called for a null instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then: "A 404 is returned"
        response.redirectedUrl == '/todo/index'
        flash.message != null

        when: "A domain instance is created"
        response.reset()
        populateValidParams(params)
        def todo = new Todo(params).save(flush: true)

        then: "It exists"
        Todo.count() == 1

        when: "The domain instance is passed to the delete action"
        controller.delete(todo)

        then: "The instance is deleted"
        Todo.count() == 0
        response.redirectedUrl == '/todo/index'
        flash.message != null
    }

}
