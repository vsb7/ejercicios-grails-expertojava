def factorial
factorial = { int n, def accu = 1G ->
    if (n < 2) return accu
    factorial.trampoline(n - 1, n * accu)
}
factorial = factorial.trampoline()

[1,2,3,4,5,6].each {
    println "${factorial it}"
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

def ayer = {Date today -> return today - 1}
def manyana = {Date today -> return today + 1}

listaFechas = [new Date().parse("d/M/yyyy H:m:s","26/7/1977 00:30:20"),
new Date().parse("d/M/yyyy H:m:s","29/7/1978 00:30:20"),
new Date().parse("d/M/yyyy H:m:s","02/3/1981 00:30:20"),
new Date().parse("d/M/yyyy H:m:s","04/2/1949 00:30:20"),
new Date().parse("d/M/yyyy H:m:s","12/11/1954 00:30:20")]

listaFechas.each{
    println "anterior a ${it} -> ${ayer it}"
    println "posterior a ${it} -> ${manyana it}"
}







