class Libro {
    String nombre
    Integer anyo
    String autor    
    String editorial
        
    String getAutor(){    
        def tmp = autor.tokenize(",");
    
        tmp[0]=tmp[0].trim();
        tmp[1]=tmp[1].trim();
        
        return tmp[1] +" "+ tmp[0]
    }
}

def l1 = new Libro(nombre:"La colmena",anyo:1951, autor:"Cela Trulock, Camilo José")
def l2 = new Libro(nombre:"La galatea",anyo:1585, autor:"de Cervantes Saavedra, Miguel")
def l3 = new Libro(nombre:"La dorotea",anyo:1632, autor:"Lope de Vega y Carpio, Félix Arturo")

/* Crea aquí las tres instancias de libro l1, l2 y l3 */

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */
l1.setEditorial("Anaya")
l2.setEditorial("Planeta")
l3.setEditorial("Santillana")

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'