class Calculadora{

    def suma(int x, int y){return x+y}
    def resta(int x, int y){return x-y}
    def mult(int x, int y){return x*y}
    def div(int x, int y){return x/y}    
}


String operador = ""
//int op1, op2

System.in.withReader {
            print 'Introduzca operador: '
            op1 = it.readLine()
            println(op1)
            
            print 'Introduzca operador: '
            op2 = it.readLine()
            println(op2)
            
            print 'Introduzca operación: '
            operador = it.readLine()
            println(operador)            
        }

def o1 = op1.toInteger();
def o2 = op2.toInteger();
        
def calc = new Calculadora()

switch(operador){
    case '+': def resultado = calc.suma(o1,o2); break
    case '-': def resultado = calc.resta(o1,o2); break
    case '/': def resultado = calc.div(o1,o2); break
    case '*': def resultado = calc.mult(o1,o2); break
}
        
        
     