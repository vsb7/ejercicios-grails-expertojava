/*
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Todo {
    private String titulo;
    private String descripcion;

    public Todo() {}

    public Todo(String tit, String des) {
        this.titulo = tit;
        this.descripcion = des;
    }

    public String getTitulo(){
        return titulo;
    }

    public void setTitulo(String tit){
        this.titulo = tit;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public void setDescripcion(String des){
        this.descripcion = des;
    }

    public static void main (String[] args){
        List todos = new ArrayList();
        todos.add(new Todo("Lavadora","Poner lavadora"));
        todos.add(new Todo("Impresora","Comprar cartuchos impresora"));
        todos.add(new Todo("Películas","Devolver películas videoclub"));

        for (Iterator iter = todos.iterator();iter.hasNext();) {
            Todo todo = (Todo)iter.next();
            System.out.println(todo.getTitulo()+" "+todo.getDescripcion());
        }
    }
}
*/
class Todo {
    String titulo
    String descripcion
}

def todos = []

todos += new Todo(titulo:"Lavadora",descripcion:"Poner lavadora")
todos += new Todo(titulo:"Impresora",descripcion:"Comprar cartuchos impresora")
todos += new Todo(titulo:"Películas",descripcion:"Devolver películas videoclub")

todos.each{
    todo ->
    println "${todo.getTitulo()} ${todo.getDescripcion()}"
}

return



